package constants

type MailInfoForgotPassword struct {
	FullName    string
	URLRedirect string
}
