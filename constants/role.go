package constants

const (
	SUPPER_ADMIN = 0
	ADMIN = 1
	NORMAL_USER = 2
	TEACHER = 3
)
const DEFAULT_ROLE = NORMAL_USER
type ROLE int