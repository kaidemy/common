package constants

import "github.com/dgrijalva/jwt-go"

var JwtSecret = []byte("secret-key")

type CustomClaims struct {
	jwt.StandardClaims
	Subject int `json:"sub,omitempty"`
}