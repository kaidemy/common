package constants

import "errors"

var (
	ErrUnAuthorize = errors.New("Unauthorize")
	ErrServer      = errors.New("Error server")
)
