package utils

import (
	"github.com/gofiber/fiber/v2"
)

func GetUserId(c *fiber.Ctx) int {
	userId, _ := c.Locals("userId").(int)
	return userId
}
