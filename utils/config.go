package utils

import (
	"fmt"
	"log"
	"github.com/spf13/viper"
)

type Configurations struct {
	SecretJWTKey   string `mapstructure:"SECRET_JWT"`
	AccessKeyMinIO string `mapstructure:"ACCESS_KEY_MINIO"`
	SecretKeyMinIO string `mapstructure:"SECRET_KEY_MINIO"`
	MinIOHost      string `mapstructure:"MINIO_HOST"`
	RabbitMQHost   string `mapstructure:"RABBIT_MQ_HOST"`
}

var EnvConfigs *Configurations

func InitConfigurations(path string) {
	v := viper.New()
	v.SetConfigFile(".env")
    v.SetConfigType("env")
	v.AutomaticEnv()

	err := v.ReadInConfig()
    if err != nil {
        log.Fatal("Error read config .env: ", err)
    }
	fmt.Println("Get Secret: ", v.GetString("RABBIT_MQ_HOST"))
	err = v.Unmarshal(&EnvConfigs)

	if err != nil {
        log.Fatal("Error unmashal config .env: ", err)
    }
	// configs := &Configurations{
	// 	SecretJWTKey:   viper.GetString("SECRET_JWT"),
	// 	AccessKeyMinIO: viper.GetString("ACCESS_KEY_MINIO"),
	// 	SecretKeyMinIO: viper.GetString("SECRET_KEY_MINIO"),
	// 	MinIOHost:      viper.GetString("MINIO_HOST"),
	// 	RabbitMQHost:   viper.GetString("RABBIT_MQ_HOST"),
	// }
	// EnvConfigs = configs
}
