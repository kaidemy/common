package middleware

import (
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kaidemy/common/constants"
)

// Define your JWT secret key


// AuthMiddleware is the middleware function
func AuthMiddleware(c *fiber.Ctx) error {
	authHeader := c.Get("Authorization")

	// Check if the Authorization header is present
	if authHeader == "" {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Unauthorized",
		})
	}
	// Extract the token from the header
	tokenString := strings.Replace(authHeader, "Bearer ", "", 1)

	claims := &constants.CustomClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return constants.JwtSecret, nil
	})

	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Unauthorized",
		})
	}

	if !token.Valid {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Invalid token",
		})
	}

	c.Locals("userId", claims.Subject)

	return c.Next()
}
