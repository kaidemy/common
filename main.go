package main

import ( // "log"
	// "gitlab.com/kaidemy/common/services/rabbitmq"
	"fmt"
	"log"

	"gitlab.com/kaidemy/common/services/rabbitmq"
	// "gitlab.com/kaidemy/common/utils"
)

// "fmt"
// "os"

// "github.com/gofiber/fiber/v2"
// "gitlab.com/kaidemy/common/constants"
// "gitlab.com/kaidemy/common/middleware"
// "gitlab.com/kaidemy/common/utils"

func main() {
	// utils.InitConfigurations(".")
	connRabbitMq, err := rabbitmq.ConnectRabbitMQ()

	if err != nil {
		log.Fatal("Cannot connect RabbitMQ", err)
	}
	
	fmt.Println("Rabbit Host: 1 ", connRabbitMq)

	// fmt.Println("Conn: ", connRabbitMq)
	// // Use the AuthMiddleware for protected routes
	// app.Get("/protected", middleware.AuthMiddleware, func(c *fiber.Ctx) error {

	// 	return c.JSON(fiber.Map{"message": utils.GetUserId(c)})
	// })

	// app.Get("/", func(c *fiber.Ctx) error {
	// 	token := services.Encode(1, constants.NORMAL_USER)
	// 	return c.Status(200).JSON(fiber.Map{"message": token})
	// })
	
	// app.Listen(":8080")
}
