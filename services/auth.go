package services

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/kaidemy/common/constants"
)

func Encode(id int, role constants.ROLE) string {
	claims := jwt.MapClaims{
		"sub":  id,
		"role": role,
		"exp":  time.Now().Add(time.Hour * 72).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString(constants.JwtSecret)
	if err != nil {
		fmt.Println(err)
	}
	return t
}

func EncodeEmail(email string) string {
	claims := jwt.MapClaims{
		"sub":  email,
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString(constants.JwtSecret)
	if err != nil {
		fmt.Println(err)
	}
	return t
}
