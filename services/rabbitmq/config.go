package rabbitmq

import (
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

type MailForgotPassword struct {
	Email string `json:"email"`
	Token string `json:"token"`
}
var (
	RABBIT_MQ_URL = os.Getenv("RABBIT_MQ_URL")
)
func ConnectRabbitMQ() (*amqp.Connection, error) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")

	if err != nil {
		return nil, err
	}

	return conn, nil
}
